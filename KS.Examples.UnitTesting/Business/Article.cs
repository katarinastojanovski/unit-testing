﻿namespace Business
{
    public class Article
    {
        public Article(int id, string description)
        {
            Id = id;
            Description = description;
        }

        public int Id { get; }
        public string Description { get; set; }
    }
}
