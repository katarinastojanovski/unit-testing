﻿using System.Collections.Generic;

namespace Business
{
    public class ArticleEqualityComparer : IEqualityComparer<Article>
    {
        public bool Equals(Article x, Article y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Id == y.Id && x.Description == y.Description;
        }

        public int GetHashCode(Article obj)
        {
            unchecked { return (obj.Id * 397) ^ obj.Description.GetHashCode(); }
        }
    }
}
