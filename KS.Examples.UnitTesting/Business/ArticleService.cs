﻿namespace Business
{
    public class ArticleService
    {
        private readonly IArticleRepository myRepository;

        public ArticleService(IArticleRepository myRepository)
        {
            this.myRepository = myRepository;
        }

        public Article GetById(int id)
        {
            return this.myRepository.GetById(id);
        }
    }
}
