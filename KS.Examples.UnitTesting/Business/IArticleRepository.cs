﻿namespace Business
{
    public interface IArticleRepository
    {
        Article GetById(int id);
    }
}
