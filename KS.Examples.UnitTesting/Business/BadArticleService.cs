﻿namespace Business
{
    // Bad code, used for blogging purposes only
    public class BadArticleService
    {
        private readonly IArticleRepository articleRepository;

        public BadArticleService(IArticleRepository articleRepository)
        {
            this.articleRepository = articleRepository;
        }

        public Article GetById(int id)
        {
            var repositoryArticle = this.articleRepository.GetById(id);
            repositoryArticle.Description = "test description";
            return repositoryArticle;
        }
    }
}
