using Moq;
using Xunit;

namespace Business
{
    public class ArticleServiceTests
    {
        [Fact]
        public void GetByIdReturnsArticle()
        {
            // Arrange
            const int id = 1;
            const string description = "test";
            var article = new Article(id, description);
            var expectedArticle = new Article(id, description);
            var articleRepositoryMock = new Mock<IArticleRepository>();
            articleRepositoryMock.Setup(x => x.GetById(id)).Returns(article);

            var articleService = new ArticleService(articleRepositoryMock.Object);

            // Act
            var actualArticle = articleService.GetById(id);

            // Assert
            Assert.Equal(expectedArticle, actualArticle, new ArticleEqualityComparer());
        }
    }
}
