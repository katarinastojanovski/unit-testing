using Moq;
using Xunit;

namespace Business
{
    // Bad code, used for blogging purposes only
    public class BadArticleServiceTests
    {
        [Fact]
        public void GetByIdReturnsArticle()
        {
            // Arrange
            const int id = 1;
            const string description = "test";
            var article = new Article(id, description);
            var articleRepositoryMock = new Mock<IArticleRepository>();
            articleRepositoryMock.Setup(x => x.GetById(id)).Returns(article);

            var articleService = new BadArticleService(articleRepositoryMock.Object);

            // Act
            var actualArticle = articleService.GetById(id);

            // Assert
            Assert.Equal(article, actualArticle);
        }
    }
}
